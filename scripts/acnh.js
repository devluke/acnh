let toolbarClosed = false;

function refreshBg() {
    const number = Math.floor(Math.random() * 104) + 1;
    const url = `images/backgrounds/bg${number}.jpg`;
    const image = new Image();
    image.onload = () => {
        document.body.style.backgroundImage = `url(${url})`;
    };
    image.src = url;
}

refreshBg();

const releaseDate = new Date("March 20, 2020 EST").getTime();

function refreshTimer() {
    const now = new Date().getTime();
    const distance = releaseDate - now;

    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById("days").textContent = days.toString();
    document.getElementById("hours").textContent = hours.toString();
    document.getElementById("minutes").textContent = minutes.toString();
    document.getElementById("seconds").textContent = seconds.toString();

    document.getElementById("days-s").textContent = days === 1 ? "" : "s";
    document.getElementById("hours-s").textContent = hours === 1 ? "" : "s";
    document.getElementById("minutes-s").textContent = minutes === 1 ? "" : "s";
    document.getElementById("seconds-s").textContent = seconds === 1 ? "" : "s";

    if (distance < 0) {
        clearInterval(interval);
        document.getElementById("timer").innerHTML = "<b>THE WAIT IS OVER!</b>";
    }
}

const interval = setInterval(refreshTimer);

refreshTimer();

setInterval(() => {
    const height = window.innerHeight;
    const closed = height < 500 || toolbarClosed;
    document.getElementById("bottom").style.display = closed ? "none" : "initial";
});

function closeToolbar() {
    toolbarClosed = true;
}
